"use strict"

// Set up
var express = require('express');
var app = express();
var mongoose = require('mongoose');
var logger = require('morgan');
var bodyParser = require('body-parser');
var cors = require('cors');
var CodeGenerator = require('node-code-generator');
var axios = require('axios');
var moment = require('moment');
var cron = require('node-cron');

/*Sockets*/
var http = require('http').Server(app);
let io = require('socket.io')(http);

//SOCKETS
io.sockets.on('connection', (socket) => {
  console.log('Client Connection recieved ID: ' + socket.id);
  socket.room = null;
  socket.host = false;

  socket.on('disconnect', function(roomCodeP, userId){
    console.log('Client disconnected ID: ' + socket.id);

    if(socket.host){
        socket.alive = false;
        io.sockets.in(roomCodeP).emit('hostLostConnection', []);

        setTimeout(function() {
            if(!socket.alive){
                deleteRoom(socket.room);
                io.sockets.in(roomCodeP).emit('hostDisconnected', []);
            }
        }, 600000);
    }else{
        io.sockets.in(socket.room).emit('clientDisconnected', []);
    }
  });

  socket.on('create', function(roomCodeP,userId){
      console.log("Creating Room", userId);
      socket.join(roomCodeP);
      socket.host = true;
      socket.userId = userId;
      socket.room = roomCodeP;

      if(!socket.alive){
          socket.alive = true;
          io.sockets.in(roomCodeP).emit('hostReconnect', socket.id);
      }
  });

  socket.on('join', function(roomCodeP, userId){
      console.log("joining Room", userId);
      socket.join(roomCodeP);
      socket.userId = userId;
      socket.host = false;
      socket.room = roomCodeP;
      //Send this event to everyone in the room.
      io.sockets.in(roomCodeP).emit('connectToRoom', socket.id);
  });

  socket.on('rejoin', function(){
      console.log("rejoin");
      if(socket.room != null){
        console.log("Broadcast to self");
        socket.broadcast.to(socket.id).emit('rejoinAvailable', socket.room);
      }
  });

  socket.on('stayAlive', function(socketId){
     console.log("stay alive recieved", socketId, socket.id);
     socket.emit('stayAliveRecieve', socket.id);
  });

  socket.on('leave', function(roomCodeP){
     console.log("Leaving room:", socket.room);
    io.sockets.in(socket.room).emit('clientDisconnected', []);

    console.log("User ID", socket.userId)
    if(socket.userId){
        deleteUser(socket.userId);
    }

    if(socket.host && socket.room != null){
        deleteRoom(socket.room);
        io.sockets.in(roomCodeP).emit('hostDisconnected', []);
    }

    socket.disconnect();
  });

  socket.on('queueSend', function(data, socketId){
    console.log("sending queue to", socketId);
    socket.broadcast.to(socketId).emit('queueRecieve', data);
  });

  socket.on('playingSend', function(data, socketId){
    console.log("sending playing to", socketId);
    socket.broadcast.to(socketId).emit('playingRecieve', data);
  });

  socket.on('librarySend', function(data, socketId){
    console.log("sending library to", socketId);
    socket.broadcast.to(socketId).emit('LibraryRecieve', data);
  });

  socket.on('clientSend', function(data, socketId){
    console.log("sending client count to", socketId);
    socket.broadcast.to(socketId).emit('clientConnectRecieve', data);
  });

  socket.on('search', function(data, roomCodeP){
    console.log("search request made from client; Search term: ", data.term, "socket", socket.id);
    var search = {};
    search.term = data.term;
    search.offset = data.offset
    search.socketId = socket.id;
    io.sockets.in(roomCodeP).emit('searchRequests', search);
  });

  socket.on('searchSend', function(data, socketId){
    console.log("Sending search results to client", socketId);
    socket.broadcast.to(socketId).emit('searchResults', data);
  });

  socket.on('suggested', function(data, roomCodeP){
      io.sockets.in(roomCodeP).emit('suggestedRequests', socket.id);
  });

  socket.on('suggestedSend', function(data, socketId){
    console.log("Sending suggested results to client", socketId);
    socket.broadcast.to(socketId).emit('suggestedResults', data);
  });

  socket.on('clientConnect', function(data, roomCodeP){
    io.sockets.in(roomCodeP).emit('clientConnectRecieve', data);
  });

  socket.on('clientDisconnect', function(data, roomCodeP){
    io.sockets.in(roomCodeP).emit('clientDisconnected', data);
  });

  socket.on('setPlaying', function(data, roomCodeP){
    socket.broadcast.to(roomCodeP).emit('playing', data);
  });

  socket.on('add', function(data, roomCodeP){
    io.sockets.in(roomCodeP).emit('queueAdd', data);
  });

  socket.on('toggleVoting', function(data, roomCodeP){
    io.sockets.in(roomCodeP).emit('toggleVotingRecieve', data);
  });

  socket.on('vote', function(data, roomCodeP){
    io.sockets.in(roomCodeP).emit('voteRecieve', data);
  });

  socket.on('remove', function(data, roomCodeP){
    socket.broadcast.to(roomCodeP).emit('queueRemove', data);
  });

  socket.on('clientRemove', function(data, roomCodeP){
    io.sockets.in(roomCodeP).emit('clientQueueRemove', data);
  });

  socket.on('trackPlay', function(data, roomCodeP){
    socket.broadcast.to(roomCodeP).emit('trackPlayRecieve', data);
  });

  socket.on('trackPause', function(data, roomCodeP){
    socket.broadcast.to(roomCodeP).emit('trackPauseRecieve', data);
  });

  socket.on('trackScrub', function(data, roomCodeP){
    socket.broadcast.to(roomCodeP).emit('trackScrubRecieve', data);
  });

  socket.on('reorder', function(data, roomCodeP){
    socket.broadcast.to(roomCodeP).emit('queueReorder', data);
  });

});

http.listen(5000, () => {
  console.log('Sockets running on port 5000');
});

// Configuration
mongoose.connect('mongodb://localhost/squawkbox');

app.use(bodyParser.urlencoded({extended: false})); // Parses urlencoded bodies
app.use(bodyParser.json()); // Send JSON responses
app.use(logger('dev')); // Log requests to API using morgan
app.use(cors());
app.enable('trust proxy');
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'DELETE, PUT');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// Models
var User = mongoose.model('User', {
    name: String,
    room: String,
});

// Models
var Room = mongoose.model('Room', {
    ip: String,
    code: String,
    start_date: { type: Date, default: Date.now }
});

/**
 * Auth API
 */
var scopes = ["streaming", "user-read-currently-playing", "user-modify-playback-state", "user-read-playback-state"],
    redirectUri = 'http://localhost/callback',
    clientId = 'da0907ac05cc42fab3ba2bcddaf0ce89',
    state = 'some-state-of-my-choice';

var SpotifyWebApi = require('spotify-web-api-node');

// Setting credentials can be done in the wrapper's constructor, or using the API object's setters.
var spotifyApi = new SpotifyWebApi({
    redirectUri: redirectUri,
    clientSecret: 'fb69ac93bdb84335990fe152c32d4e4c',
    clientId: clientId
});

//CRONS
cron.schedule('0 22 * * *', function() {
    console.log("Deleting rooms");
    var min_date = moment().subtract(1, "days");

    Room.remove({ start_date : {"$lt" : min_date.toDate()} }, function(err, room) {});
});

app.get('/api/auth/token/:code', function(req, res){
    // The code that's returned as a query parameter to the redirect URI
    var code = req.params.code;

    // Retrieve an access token and a refresh token
    spotifyApi.authorizationCodeGrant(code).then(
    function(data) {
        console.log('The token expires in ' + data.body['expires_in']);
        console.log('The access token is ' + data.body['access_token']);
        console.log('The refresh token is ' + data.body['refresh_token']);

        // Set the access token on the API object to use it in later calls
        //spotifyApi.setAccessToken(data.body['access_token']);
        //spotifyApi.setRefreshToken(data.body['refresh_token']);

        res.json({token: data.body['access_token'], refreshToken: data.body['refresh_token'], expiresIn: data.body['expires_in']});
    },
    function(err) {
        console.log('Something went wrong!', err);
        res.send(err);
    }
    );
});

app.get('/api/auth/tokenRefresh/:token', function(req, res){
    var token = req.params.token;
    spotifyApi.setRefreshToken(token);
    spotifyApi.refreshAccessToken().then(
        function(data) {
            console.log(data.body);

            // Save the access token so that it's used in future calls
            //spotifyApi.setAccessToken(data.body['access_token']);
            res.json({token: data.body['access_token'], expiresIn: data.body['expires_in']});
        },
        function(err) {
            console.log('Could not refresh access token', err);
            res.send(err);
        }
    );
});


/**
 * Music API
 */

app.get('/api/music/firstSong', function(req, res){
    pm.getAllTracks(function(err, library) {
        if (err) {
            res.send(err);
        } else {
            var song = library.data.items.pop();
            res.json(song);
        }
    });
});

app.get('/api/music/library/:provider', function(req, res){
    if(req.params.provider==1){
        pm.getAllTracks(function(err, library) {
            if (err || !library.hasOwnProperty('data')) {
                res.status(403).send({ error: 'No music found.' });
            } else {
                res.json(library.data.items);
            }
        });
    }else{
        // Get tracks in the signed in user's Your Music library
        spotifyApi.getMySavedTracks()
        .then(function(data) {
            console.log(data.body.items);
            res.json(data.body.items);
        }, function(err) {
            res.send(err);
        });
    }

});

app.get('/api/music/getStream/:id',  function(req, res){
     pm.getStreamUrl(req.params.id, function(err, streamUrl) {
        if (err) {
            res.send(err);
        } else {
            res.json(streamUrl);
        }
    });
});

app.get('/api/music/search/:term/:offset/:token',  function(req, res){
    console.log(req.params.token);
     search(req.params.term, req.params.offset, req.params.token).then(songs =>{
        res.json(songs);
     }).catch(err => {
        res.send(err);
     });
});

function search(term, offset, token){
    return new Promise((resolve, reject)=>{
        console.log("Searching term:", term, token);
        spotifyApi.setAccessToken(token);
        spotifyApi.searchTracks(term, { limit: 20, offset:  offset})
        .then(function(data) {
            console.log('Search by term', data.body);
            resolve(data.body.tracks.items);
        }, function(err) {
            console.error(err);
            reject(err);
        });
    });
}


/**
 * Users API
 */

// Get Users
app.get('/api/users', function(req, res) {

    console.log("fetching all users");

    // use mongoose to get all users in the database
    User.find(function(err, users) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err)
            res.send(err)

        res.json(users); // return all users in JSON format
    });
});

//Get Users with room code
app.get('/api/users/:room', function(req, res) {

    console.log(req.params.room);
    // use mongoose to get all users with room code
    User.find({ room: req.params.room },function(err, users) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err)
            res.send(err)

        res.json(users); // return all users in JSON format
    });
});

// create user and send back all users after creation
app.post('/api/user', function(req, res) {

    console.log("creating user");

    // create a user, information comes from request from Ionic
    User.create({
        name : req.body.name,
        room : req.body.room
    }, function(err, user) {
        if (err)
            res.send(err);

        res.json(user);
    });

});

// delete a user
app.delete('/api/user/:user_id', function(req, res) {
    User.remove({
        _id : req.params.user_id
    }, function(err, user) {

    });
});

function deleteUser(idp){
    return new Promise((resolve, reject)=>{
       User.remove({
        _id : idp
    }, function(err, user) {
        if(err)
            reject(err);

        console.log("Deleting user with id:" + idp);
        resolve(user);
        });
    });
}

/**
 * Rooms API
 */

// Get rooms
app.get('/api/rooms', function(req, res) {

    console.log("fetching all rooms");

    // use mongoose to get all rooms in the database
    Room.find(function(err, rooms) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err)
            res.send(err)

        res.json(rooms); // return all rooms in JSON format
    });
});

// create room and send back all rooms after creation
app.post('/api/room', function(req, res) {
    var generator = new CodeGenerator();
    var generated_code = generator.generateCodes('******', 1, {'alphanumericChars' : 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'});
    var ip_addr = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

    if (ip_addr.substr(0, 7) == "::ffff:") {
      ip_addr = ip_addr.substr(7)
    }

    console.log("creating room with ip " + req.connection.remoteAddress + "and the code is " + generated_code);
    // create a room, information comes from request from Ionic
    Room.create({
        ip: ip_addr,
        code: generated_code,
        start_date: new Date()
    }, function(err, room) {
        if (err)
            res.send(err);

        res.json(room);
    });

});

//Check for a room on the same ip
app.get('/api/room/ip', function(req, res) {
    var ip_addr = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

    if (ip_addr.substr(0, 7) == "::ffff:") {
      ip_addr = ip_addr.substr(7)
    }

    // use mongoose to get all users with room code
    Room.findOne({ ip: ip_addr }, {}, { sort: { 'start_date' : -1 } }, function(err, room) {
      // if there is an error retrieving, send the error. nothing after res.send(err) will execute
      if(err)
          res.send(err);

      res.json(room);
    });

});

//Get room by id
app.get('/api/room/:code',  function(req, res) {
   Room.findOne({ code: req.params.code }, function (err, room){
        if(err)
            res.send("Room not found");

        res.json(room);
    });
});

function deleteRoom(code){
    console.log("Deleting room with code:", code);
    return new Promise((resolve, reject)=>{
        Room.findOne({ code: code }, function (err, room){
            if(err)
               reject(err);

            console.log("Deleting room:", room);
            if(room != null) {
              Room.remove({
                _id : room._id
              }, function(err, room) {
                  resolve(room);
              });
            }
        });
    });
}

// delete a room
app.delete('/api/room/:room_id', function(req, res) {
    Room.remove({
        _id : req.params.room_id
    }, function(err, room) {

    });
});

// listen
app.listen(8080);
console.log("App listening on port 8080");
