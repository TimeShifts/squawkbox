'use strict';

module.exports = function() {
    var app = express();
    var http = require('http').Server(app);
    let io = require('socket.io')(http);

    //SOCKETS
    io.sockets.on('connection', (socket) => {
    console.log('Client Connection recieved ID: ' + socket.id);
    socket.room = null;
    socket.host = false;

    socket.on('disconnect', function(){
        console.log('Client disconnected ID: ' + socket.id);
        io.sockets.in(socket.room).emit('clientDisconnected', []);

        console.log("User ID", socket.userId)
        if(socket.userId)
            deleteUser(socket.userId);

        if(socket.host && socket.room != null)
            deleteRoom(socket.room);

        //socket.disconnect();
    });

    socket.on('create', function(roomCodeP,userId){
        console.log("Creating Room", userId);
        socket.join(roomCodeP);
        socket.host = true;
        socket.userId = userId;
        socket.room = roomCodeP;
    });

    socket.on('join', function(roomCodeP, userId){
        console.log("joining Room", userId);
        socket.join(roomCodeP);
        socket.userId = userId;
        socket.host = false;
        socket.room = roomCodeP;
        //Send this event to everyone in the room.
        io.sockets.in(roomCodeP).emit('connectToRoom', socket.id);
    });

    socket.on('rejoin', function(){
        console.log("rejoin");
        if(socket.room != null){
            console.log("Broadcast to self");
            socket.broadcast.to(socket.id).emit('rejoinAvailable', socket.room);
        }
    });

    socket.on('leave', function(roomCodeP){
        console.log("Leaving room:", socket.room);
        socket.leave(roomCodeP);

        if(socket.host){
            io.sockets.in(roomCodeP).emit('hostDisconnected', []);
            socket.room = null;
            deleteRoom(roomCodeP);
        }else{
            //Send this event to everyone in the room.
            io.sockets.in(roomCodeP).emit('clientDisconnected', []);
        } 
    });

    socket.on('queueSend', function(data, socketId){
        console.log("sending queue to", socketId);
        socket.broadcast.to(socketId).emit('queueRecieve', data);
    });

    socket.on('playingSend', function(data, socketId){
        console.log("sending playing to", socketId);
        socket.broadcast.to(socketId).emit('playingRecieve', data);
    });

    socket.on('librarySend', function(data, socketId){
        console.log("sending library to", socketId);
        socket.broadcast.to(socketId).emit('LibraryRecieve', data);
    });

    socket.on('clientSend', function(data, socketId){
        console.log("sending client count to", socketId);
        socket.broadcast.to(socketId).emit('clientConnectRecieve', data);
    });

    socket.on('clientConnect', function(data, roomCodeP){
        io.sockets.in(roomCodeP).emit('clientConnectRecieve', data);
    });

    socket.on('clientDisconnect', function(data, roomCodeP){
        io.sockets.in(roomCodeP).emit('clientDisconnected', data);
    });

    socket.on('setPlaying', function(data, roomCodeP){
        socket.broadcast.to(roomCodeP).emit('playing', data);
    });

    socket.on('add', function(data, roomCodeP){
        io.sockets.in(roomCodeP).emit('queueAdd', data);
    });

    socket.on('remove', function(data, roomCodeP){
        socket.broadcast.to(roomCodeP).emit('queueRemove', data);
    });

    socket.on('reorder', function(data, roomCodeP){
        socket.broadcast.to(roomCodeP).emit('queueReorder', data);
    });

    });

    http.listen(5000, () => {
    console.log('socket on port 5000');
    });

};