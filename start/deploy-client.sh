#!/bin/bash

### Configuration ###

APP_DIR=/var/www/squawkbox
GIT_URL=git@gitlab.com:TimeShifts/squawkbox.git

printf '\n\n------------------------------------------'
printf '\nRunning SquawkBox Client Deployment Script'
printf '\n------------------------------------------'

# Pull latest code
printf '\n\nPulling latest code from '
printf $GIT_URL
printf '\n'

if [ -d $APP_DIR ]; then
  cd $APP_DIR
  git pull
else
  git clone $GIT_URL $APP_DIR
  cd $APP_DIR
fi

# Install dependencies
printf '\n\nInstalling Dependencies\n'
npm install
npm prune

# Run production build
printf '\n\nRunning Production Build\n'
npm run build --prod

# Restart nginx
printf '\n\nRestarting Nginx\n'
service nginx restart

printf '\n\nClient has been updated '
printf '\n\n'
