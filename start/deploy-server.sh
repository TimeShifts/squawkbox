#!/bin/bash

### Configuration ###

APP_DIR=/var/www/squawkbox
GIT_URL=git@gitlab.com:TimeShifts/squawkbox.git


printf '\n\n------------------------------------------'
printf '\nRunning SquawkBox Server Deployment Script'
printf '\n------------------------------------------'

# Stop Forever Process
printf "\n\nStopping all instances of the server\n"
forever stopall

# Pull latest code
printf '\n\nPulling latest code from '
printf $GIT_URL
printf '\n'

if [ -d $APP_DIR ]; then
  cd $APP_DIR
  git pull 
else
  git clone $GIT_URL $APP_DIR
  cd $APP_DIR
fi

# Install dependencies
printf '\n\nInstalling Dependencies\n'
npm install
npm prune

# Restart Forever Process
printf '\n\nRestarting Forever Process\n'
cd $APP_DIR/server
touch $APP_DIR/server/logs/out.log
touch $APP_DIR/server/logs/err.log

forever start -o $APP_DIR/server/logs/out.log -e $APP_DIR/server/logs/out.log server.js

printf '\n\nServer is live. Logs can be found at '
printf $APP_DIR/server/logs
printf '\n\n'
