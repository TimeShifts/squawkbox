import { Injectable } from '@angular/core';
import { AlertController, ToastController, LoadingController } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';

declare var window: any;

@Injectable()
export class AppService {

  toastTime = 1000;
  loading:any;
  name: string;

  constructor(private alertCtrl: AlertController,private storage: Storage, public platform: Platform, public toastCtrl: ToastController, public iab: InAppBrowser, public loadingCtrl: LoadingController) {

  }

  showToast(message, time?){
    if(!time){
        time = this.toastTime;
    }

    let toast: any;

      if(window.iphoneX == true) {
        toast = this.toastCtrl.create({
          message: message,
          duration: time,
          cssClass: "iphonex-toast"
        });
      }
      else {
        toast = this.toastCtrl.create({
          message: message,
          duration: time
        });
      }

    toast.present();
  }

  showError(text) {
    this.loading.dismiss();

    let alert = this.alertCtrl.create({
      title: 'Oops..',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  createErrorAlert(text){
    let alert = this.alertCtrl.create({
        title: 'Oops..',
        subTitle: text,
        buttons: ['OK']
      });
      alert.present(prompt);
      return alert;
  }


  showErrorWithRedirect(buttonText, url, text) {
    this.loading.dismiss();

    let alert = this.alertCtrl.create({
      title: 'Oops..',
      subTitle: text,
      buttons: [ {
        text: buttonText,
        handler: () => {
          this.redirect(url);
        }
      }]
    });
    alert.present(prompt);
  }

  showTrackAddToast(time?){
    this.showToast("Track added to queue", time);
  }

  redirect(urlP){
     if(this.platform.is('cordova')){
        //let browser = this.iab.create(urlP, '_system');
      }else{
        window.location.href = urlP;
      }
  }

  showLoading(message?){
    if(!message)
        message = 'Please wait...'

    this.loading = this.loadingCtrl.create({
      content: message
    });

    this.loading.present();
  }

  stopLoading(){
    this.loading.dismiss();
  }

  //Local storage
  setItem(name, object){
    this.name = object;
    this.storage.set(name, object);
  }

  getItem(name): any{

    return new Promise((resolve, reject) => {
      this.storage.get(name).then((val) => {
        resolve(val);
      }).catch((err)=>{
        resolve([]);
      });
    });
  }

  setTokens(data){
    localStorage.setItem ('token', data.token);
    if(data.refreshToken){
      localStorage.setItem ('refreshToken', data.refreshToken);
    }
    var now = Date.now();
    var expiryDate = now + (data.expiresIn * 1000);
    localStorage.setItem ('expires', ''+expiryDate);
  }

  isTokenExpired(){
    var token = localStorage.getItem ('token');
    var expires = localStorage.getItem ('expires');


    var now = new Date().getTime();

     console.log(+expires, now);
    if(token == null || +expires < now){
      return true;
    }else{
      return false;
    }
  }

}
