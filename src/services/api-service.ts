import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Platform } from 'ionic-angular';

import 'rxjs/add/operator/map';

@Injectable()
export class ApiService {

  url: string;
  socketUrl: string;
  apiPort: number = 8080;
  socketPort: number = 5000;

  constructor(public http: Http, public platform: Platform) {
    var host;

    if (this.platform.is('cordova')) {
      host = "http://138.197.143.214:";
      this.url = host + this.apiPort;
      this.socketUrl = host + this.socketPort;
    } else {
      host = "https://squawkbox.app" ;
      this.url = host;
      this.socketUrl = host;
    }
  }
}
