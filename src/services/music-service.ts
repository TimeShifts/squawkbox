import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { ApiService } from './api-service';
import { AppService } from './app-service';
import { AuthService } from './auth-service';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Platform } from 'ionic-angular';
import { Track } from '../models/track';
import 'rxjs/add/operator/map';

declare var cordova: any;

export enum MusicProvider{
  spotify= 0
}

@Injectable()
export class MusicService {

  data: any;
  library: any[];
  provider: number;
  CLIENT_ID: string = "da0907ac05cc42fab3ba2bcddaf0ce89";
  accessToken: any;
  seed: any;

  constructor(private _http: Http, private apiService: ApiService, public iab: InAppBrowser, public appService: AppService, public platform: Platform) {
    this.data = null;

    this.provider = MusicProvider.spotify;
  }

  getRandomTrack() {

    return new Promise(resolve => {
      var track;
      if(this.library){
        track = this.pickRandomTrack();
        this.getStream(track).then(data => {
          track.src = data;
          resolve(track);
        });
      }else{
        var me = this;
        this.getLibrary().then(data =>{
          track = me.pickRandomTrack();
          me.getStream(track).then(data => {
            track.src = data;
            resolve(track);
          });
        })
      }
    });
  }

  //Returns library
  getLibrary(): Promise<any> {
    return new Promise((resolve, reject) => {

      if(this.library){
        resolve(this.library);
      }
      else{
        var obj: any;

        this._http.get(this.apiService.url + '/api/music/library/' + this.provider)
          .map(res => res.json())
          .subscribe(data => {
            for (let song of data) {
              obj = song;
              obj.src = "";
              song = obj;
            }
            this.library = data;
            resolve(data);
          },
          err => {
            console.log(err);
            reject(err.json());
          });
        }
    });
  }

  createSeeds(queue){
    this.seed = {
        limit: 3,
        seedArtists: "",
        seedTracks: ""
    };
    var index = 0;
    queue.forEach( track => {

      if(index < 5){
        this.seed.seedArtists = this.seed.seedArtists=="" ? track.artistId : this.seed.seedArtists +  "," + track.artistId;
        index++;
      }

      if(index < 5){
        this.seed.seedTracks = this.seed.seedTracks=="" ? track.id : this.seed.seedTracks +  "," + track.id;
        index++;
      }
    });
    
  }

  getSuggestedSongs(queue){
     return new Promise(resolve => {

      this.createSeeds(queue);
      var that = this;
    
      let headers = new Headers();
      let authToken = localStorage.getItem('token');
      headers.append('Authorization', 'Bearer '+authToken);
      headers.append('Content-Type', 'application/json');

      let data = new URLSearchParams();
      data.set('limit', ""+ this.seed.limit);
      data.set('seed_artists', this.seed.seedArtists);
      data.set('seed_tracks', this.seed.seedTracks);
      data.set('min_popularity', "60");

      let options = new RequestOptions({ headers: headers, params: data });

      this._http.get('https://api.spotify.com/v1/recommendations', options)
        .map(res => res.json())
        .subscribe(data=>{
           if(data.statusCode == 401){
            that.refreshToken().then(function(){
              that.getSuggestedSongs(queue).then(data=>{
                  resolve(data);
              });
            });
          }else{
            console.log("Suggested songs", data);
            resolve(this.formatTracks(data.tracks));
          }
        });
    });
  }

  getSearchResults(term, offset?) {

    if(!offset){
      offset = 0;
    }
    
    return new Promise(resolve => {
      var that = this;
      this._http.get(this.apiService.url + '/api/music/search/' + term + '/' + offset + "/"+ localStorage.getItem('token'))
        .map(res => res.json())
        .subscribe(data => {
          if(data.statusCode == 401){
            that.refreshToken().then(function(){
              that.getSearchResults(term, offset).then(data=>{
                  resolve(data);
              });
            });
          }else{
            resolve(this.formatTracks(data));
          }

        });
    });
  }

  refreshToken(){
    return new Promise((resolve, reject) => {
     this._http.get(this.apiService.url + '/api/auth/tokenRefresh/' + localStorage.getItem('refreshToken')) .map(res => res.json())
      .subscribe(data => {
          this.appService.setTokens(data);
          resolve();
      },
      err => {
        console.log(err.json());
        reject();
      });
    });
  }

  getStream(track) {
    var trackId = this.getStreamLookupId(track);
    return new Promise(resolve => {
      this._http.get(this.apiService.url + '/api/music/getStream/' + trackId)
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        });
    });
  }

  addUserToTrack(track, isHost, username?) {
    if (isHost)
      track.queuedBy = "Host";
    else
      track.queuedBy = username;

    return track;
  }

  private pickRandomTrack(){
    var index = Math.floor(Math.random() * this.library.length - 1);
    return this.library[index];
  }

  private getStreamLookupId(track) {
    if (track.storeId)
      return track.storeId;
    else if(track.nid)
      return track.nid;
    else
      return track.id
  }

  private formatTracks(tracks){
    var formattedTracks = [];
    tracks.forEach( track => {
      var newTrack = new Track();
      formattedTracks.push(newTrack.create(track));
    });
    return formattedTracks;
  }

  public initSpotifyEventManager() {
    return cordova.plugins.spotify.getEventEmitter();
  }

  public playTrack(track) {
    cordova.plugins.spotify.play(track.uri, {
      clientId: this.CLIENT_ID,
      token: localStorage.getItem('token')
    })
    .then(() => track.isPlaying = true)
    .catch(() => {});
  }

  public pauseTrack(track) {
    cordova.plugins.spotify.pause()
    .then(() => {
      track.isPlaying = false;
      setTimeout(function() {this.getTrackPosition(track)}, 1500);
    })
    .catch(() => {});
  }

  public resumeTrack(track) {
    cordova.plugins.spotify.resume()
    .then(() => {
      track.isPlaying = true;
      setTimeout(function() {this.getTrackPosition(track)}, 1500);
    })
    .catch(() => {});
  }

  public seekToOnPause(ms, track) {
    cordova.plugins.spotify.play(track.uri, {
      clientId: this.CLIENT_ID,
      token: localStorage.getItem('token')
    }, ms)
    .then(() => track.isPlaying = true)
    .catch(() => {});
  }

  public seekTo(ms, track) {
    cordova.plugins.spotify.seekTo(ms)
    .then(() => {
      setTimeout(function() {this.getTrackPosition(track)}, 1500);
    })
    .catch(() => {});
  }

  public getTrackPosition(track) {
    cordova.plugins.spotify.getPosition()
    .then(pos => { track.playPosition = pos; })
    .catch(() => {});
  }
}
