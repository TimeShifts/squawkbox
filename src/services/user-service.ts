import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { ApiService } from './api-service';
import { AppService } from './app-service';

import 'rxjs/add/operator/map';

@Injectable()
export class UserService {

  user: any;
  usersInRoom: any;

  constructor(public http: Http, public apiService: ApiService, public appService: AppService) {
    this.user = null;
    this.usersInRoom = [];
  }

  getUsersInRoom(room_codeP) {

    return new Promise(resolve => {

      this.http.get(this.apiService.url + '/api/users/' + room_codeP)
        .map(res => res.json())
        .subscribe(data => {
          this.usersInRoom = data;
          resolve(this.usersInRoom);  
        });
    });

  }

  createUser(roomP) {
    return new Promise((resolve, reject) => {
      if(this.user){
        this.user.room = roomP;
        resolve(this.user);
      }else{
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        this.getUsername().then(name =>{
          this.user = { name: name, room: roomP };

          this.http.post(this.apiService.url + '/api/user', JSON.stringify(this.user), { headers: headers })
            .map(res => res.json())
            .subscribe(data => {
              console.log("Created user", data);
              this.user = data;
              resolve(data);
            });
        });
      }
    });
  }

  getUsername(){
    return new Promise((resolve, reject) => {
      
      this.appService.getItem("name").then(data =>{
        resolve(data);
        
        if(data == "" || data == null){
          resolve("Unknown")
        }
      }).catch(err=>{
        resolve("Unknown");
      });
    });
  }

}