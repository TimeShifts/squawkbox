
import { Injectable } from '@angular/core';
import { HostService } from './host-service';
import { AppService } from './app-service';
import { MusicService } from './music-service';
import { Http, Headers, URLSearchParams } from '@angular/http';
import { OauthCordova } from 'ng2-cordova-oauth/platform/cordova'
import { Spotify } from 'ng2-cordova-oauth/provider/spotify';
import { IOAuthOptions } from 'ng2-cordova-oauth/provider';

@Injectable()
export class AuthService {

  SPOTIFY_TOKEN_URL: string = "https://accounts.spotify.com/api/token";
  GRANT_TYPE: string = "authorization_code";
  REDIRECT_URI: string =  "http://localhost/callback";
  CLIENT_ID: string = "da0907ac05cc42fab3ba2bcddaf0ce89";
  spotify: any;
  config: IOAuthOptions = {
      clientId: this.CLIENT_ID,
      appScope: ["streaming", "user-read-currently-playing", "user-modify-playback-state", "user-read-playback-state"],
      redirectUri: this.REDIRECT_URI,
      responseType: "code"
    };

  HOST = "http://138.197.143.214:8080";

  constructor(public hostService: HostService, public http: Http, public appService: AppService, private musicService: MusicService) {
      this.spotify = new Spotify(this.config);
  }


  // Spotify Auth
  spotifyAuth(){
    return new Promise((resolve, reject) => {

      this.testToken().catch(data => {
        return new OauthCordova().logInVia(this.spotify, { clearsessioncache: 'no'}).then((response: {code: string}) => {
            console.log(JSON.stringify(response.code));

            this.http.get(this.HOST + '/api/auth/token/' + response.code)
              .map(res => res.json())
              .subscribe(data => {
                  console.log(data);
                  this.appService.setTokens(data);

                  this.hostService.createRoomAndNavigate().then(data =>{
                    resolve(data);
                  });
              },
              err => {
                return reject(err.json());
              });


            return resolve(response.code);
        }, (error) => {
            console.log(JSON.stringify(error));
            return reject(error);
        });
      });
    });

  }

  testToken(){
    return new Promise((resolve, reject) => {
      if(!this.appService.isTokenExpired()){
         this.hostService.createRoomAndNavigate().then(data =>{
          resolve(data);
        });
      }else{
        if(localStorage.getItem('token') != null){
          this.musicService.refreshToken().then(data => {
              this.hostService.createRoomAndNavigate().then(data =>{
                resolve(data);
              });
            }).catch( err=>{
              reject();
          });
        }else{
          reject();
        }
      }
    });
  }
}
