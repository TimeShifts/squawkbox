import { Injectable } from '@angular/core';
import { App } from 'ionic-angular';
import { UserService } from './user-service';
import { RoomService } from './room-service';
import { HostDashboardPage } from '../pages/host-dashboard/host-dashboard';

@Injectable()
export class HostService {
    
  constructor(public roomService: RoomService, public userService: UserService, public app: App) {

  }

  createRoomAndNavigate(){
    return new Promise((resolve, reject) => {
        this.roomService.createRoom().then((data) => {
            console.log(data);
            this.userService.createUser(data).then((user:any) =>{
                console.log(user);
                this.roomService.connectSocket();
                this.roomService.createRoomSocket(data, user._id);
                
                this.app.getActiveNav().setRoot(HostDashboardPage, {
                user: user
                });
                resolve("success");
            });
            
            }).catch((err) => { 
                reject(err);
            });
    });
  }
}
