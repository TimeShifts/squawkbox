import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ApiService } from './api-service';
import { BackgroundMode } from '@ionic-native/background-mode';
import { PowerManagement } from '@ionic-native/power-management';
import { Platform } from 'ionic-angular';
import 'rxjs/add/operator/map';
import * as io from 'socket.io-client';

@Injectable()
export class RoomService {

  data: any;
  roomCode: string;
  private socket;
  isHost: boolean;
  userId: any;
  stayAliveTimer: any;

  constructor(public http: Http, public apiService: ApiService, private backgroundMode:BackgroundMode, private powerManagement: PowerManagement, public plt: Platform) {
    this.data = null;
  }

  // Join Room with room code
  joinRoom(roomCodeP, userIdP) {
    this.userId = userIdP;

    return new Promise((resolve, reject) => {
      this.roomCode = roomCodeP;

      this.getRoom(roomCodeP).then(room =>{
        console.log("Joining Room: " + roomCodeP);
        this.socket.emit('join',roomCodeP, userIdP);
        resolve();
      }).catch(err => {
        reject(err);
      });
    });

  }

  leaveRoom() {
    //disconnect socket
    this.socket.emit('leave', this.roomCode);
    clearInterval(this.stayAliveTimer);
  }

  getRoom(roomCodeP) {
    return new Promise((resolve, reject) => {

      this.http.get(this.apiService.url + '/api/room/' + roomCodeP)
        .map(res => res.json())
        .subscribe(data => {
          if(data == null){
            console.log("is null");
            reject("Room not found");
          }else{
             resolve(data.code);
          }
        }, err => {
          reject("Couldn't connect to server");
        });
    });
  }

  roomOnNetworkCheck() {
    return new Promise((resolve, reject) => {

      this.http.get(this.apiService.url + '/api/room/ip')
        .map(res => res.json())
        .subscribe(data => {
          console.log("Room on network: ", data);
          if(data != null){
            resolve(data.code);
          }else{
            reject();
          }
        }, error => {
          reject("Connection error");
        });
    });
  }

  createRoom() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return new Promise((resolve, reject) => {
      this.http.post(this.apiService.url + '/api/room', { headers: headers })
        .map(res => res.json())
        .subscribe(data => {
          this.roomCode = data.code;
          this.isHost = true;
          resolve(data.code);
        }, error => {
          reject("Couldn't connect to server");
        });
    });
  }

  createRoomSocket(room, userId){
    console.log("Emit Create Room");
    this.userId = userId;
    this.socket.emit('create', room, userId);
  }

  deleteRoom(id) {

    this.http.delete(this.apiService.url + '/api/room/' + id).subscribe((res) => {
      console.log(res.json());
    });

  }

  addUserToTrack(trackP, nameP, idP) {
    trackP.queuedBy = nameP;
    trackP.queuedById = idP;
    return trackP;
  }

  // Socket methods
  connectSocket(){
    this.enableBackgroundMode();

    //Create socket
    this.socket = io(this.apiService.socketUrl, {
        reconnection: true,
        reconnectionDelay: 1000,
        reconnectionDelayMax : 5000,
        reconnectionAttempts: 99999
    } );

    //ON CONNECT
    this.socket.on('connect', ()=> {
      console.log("Socket connected");
      this.stayAliveTimer = setInterval( () => {
        console.log("stay alive send");
        this.socket.emit('stayAlive', '');
      }, 30000);
    });

     //ON RECONNECT
    this.socket.on('reconnect', ()=> {
      console.log("Socket has reconnected rejoining room");
      if(this.isHost){
        this.createRoomSocket(this.roomCode, this.userId);
        this.sendHostReconnect();
      }else{
        this.joinRoom(this.roomCode, this.userId);
      }
    });

    //ON DISCONNECT
    this.socket.on('disconnect', ()=> {
      clearInterval(this.stayAliveTimer);
      console.log("Socket has become disconnected listening for app resume");
      this.plt.resume.subscribe(event =>{
        console.log("resuming app");
        this.socket.connect();
      });
    });
  }

  enableBackgroundMode(){
    this.backgroundMode.enable();

    this.powerManagement.acquire().then(data=>{
      console.log("Power Mana on", data);

      this.powerManagement.setReleaseOnPause(false).then(data=>{
        console.log("set release false");
      })
    })
  }

  // Socket listening events
  newConnection() {
    console.log("Waiting for new connection");
    return this.listenForSocketEvent("connectToRoom");
  }

  clientConnectRecieve(){
    console.log("Waiting for client count");
    return this.listenForSocketEvent("clientConnectRecieve");
  }

  clientDisconnected(){
    console.log("Waiting for client disconnect");
    return this.listenForSocketEvent("clientDisconnected");
  }

  hostDisconnected(){
    console.log("Waiting for host disconnect");
    return this.listenForSocketEvent("hostDisconnected");
  }

  hostReconnected(){
    console.log("Waiting for host reconnect");
    return this.listenForSocketEvent("hostReconnected");
  }

  stayAliveRecieve(){
    console.log("Waiting for stay alive");
    return this.listenForSocketEvent("stayAliveRecieve");
  }

  playingRecieve() {
    console.log("Waiting for playing recieved");
    return this.listenForSocketEvent("playingRecieve");
  }

  libraryRecieve() {
    console.log("Waiting for library recieved");
    return this.listenForSocketEvent("libraryRecieve");
  }

  queueRecieve() {
    console.log("Waiting for queue recieved");
    return this.listenForSocketEvent("queueRecieve");
  }

  toggleVotingRecieve(){
    console.log("Waiting for toggle voting change");
    return this.listenForSocketEvent("toggleVotingRecieve");
  }

  voteRecieve() {
    console.log("Waiting for votes");
    return this.listenForSocketEvent("voteRecieve");
  }

  searchRequests() {
    console.log("Waiting for search requests");
    return this.listenForSocketEvent("searchRequests");
  }

  searchResults() {
    console.log("Waiting for search reasults from host");
    return this.listenForSocketEvent("searchResults");
  }

  suggestedRequests() {
    console.log("Waiting for suggested requests");
    return this.listenForSocketEvent("suggestedRequests");
  }

  suggestedResults() {
    console.log("Waiting for suggested results");
    return this.listenForSocketEvent("suggestedResults");
  }

  playing() {
    console.log("Waiting for playing change");
    return this.listenForSocketEvent("playing");
  }

  queueAdd() {
    console.log("Waiting for queue add change");
    return this.listenForSocketEvent("queueAdd");
  }

  queueRemove() {
    console.log("Waiting for queue remove change");
    return this.listenForSocketEvent("queueRemove");
  }

  clientQueueRemove() {
    console.log("Waiting for client queue remove change");
    return this.listenForSocketEvent("clientQueueRemove");
  }

  queueReorder() {
    console.log("Waiting for queue reorder change");
    return this.listenForSocketEvent("queueReorder");
  }

  trackPlayRecieve() {
    console.log("Waiting for track play click");
    return this.listenForSocketEvent("trackPlayRecieve");
  }

  trackPauseRecieve() {
    console.log("Waiting for track pause click");
    return this.listenForSocketEvent("trackPauseRecieve");
  }

  trackScrubRecieve() {
    console.log("Waiting for track scrub change");
    return this.listenForSocketEvent("trackScrubRecieve");
  }

  // Emit Socket events to server
  clientConnect(count) {
    this.emitToRoom("clientConnect", count);
  }

  clientSend(count, socketId) {
    this.emitToUser("clientSend", count, socketId);
  }

  sendClientDisconnect() {
    this.emitToRoom("clientDisconnect", []);
  }

  sendHostReconnect(){
    this.emitToRoom("sendHostReconnect", []);
  }

  sendPlaying(playing, socketId) {
    this.emitToUser('playingSend', playing, socketId);
  }

  sendQueue(queue, socketId) {
    this.emitToUser('queueSend', queue, socketId);
  }

  toggleVoting(voting: boolean){
    console.log("toggle voting", voting);
    this.emitToRoom("toggleVoting", voting);
  }

  sendVote(track){
    console.log("sending vote");
    this.emitToRoom("vote", track);
  }

  sendLibrary(library, socketId) {
    this.emitToUser('librarySend', library, socketId);
  }

  setPlaying(track) {
    this.emitToRoom("setPlaying", track);
  }

  addToQueue(trackP, nameP, idP) {
    console.log("Adding to queue")
    trackP = this.addUserToTrack(trackP, nameP, idP);
    this.emitToRoom("add", trackP);
  }

  removeTrackFromQueue(track) {
    this.emitToRoom("remove", track);
  }

  clientRemoveTrackFromQueue(track) {
    this.emitToRoom("clientRemove", track);
  }

  reorderQueue(indexes) {
    this.emitToRoom("reorder", indexes);
  }

  search(term, offset) {
    if(!offset) {
      offset = 0;
    }
    var obj = { term: term, offset: offset};
    this.emitToRoom("search", obj);
  }

  searchSend(data, socketId) {
    this.emitToUser("searchSend", data, socketId);
  }

  suggested(){
    this.emitToRoom("suggested", '');
  }

  suggestedSend(data, socketId){
    this.emitToUser("suggestedSend", data, socketId);
  }

  trackPlay(){
    console.log("Send track play click");
    this.emitToRoom("trackPlay", '');
  }

  trackPause(){
    console.log("Send track pause click");
    this.emitToRoom("trackPause", '');
  }

  trackScrub(scrub){
    console.log("Send track scrub", scrub);
    this.emitToRoom("trackScrub", scrub);
  }

  private emitToRoom(event, data) {
    this.socket.emit(event, data, this.roomCode);
  }

  private emitToUser(event, data, socketId){
    this.socket.emit(event, data, socketId);
  }

  private listenForSocketEvent(event){
    let observable = new Observable<any>(observer => {
      this.socket.on(event, function (data) {
        observer.next(data);
      });
      return () => {
        //this.socket.disconnect();
      };
    })
    return observable;
  }

}
