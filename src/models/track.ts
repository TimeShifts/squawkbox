import { Injectable } from '@angular/core';

@Injectable()
export class Track {
  name: string = "N/A";
  imageUrl: string = "";
  imageUrlFull: string = "";
  id: string;
  artist: string = "";
  artistId: number;
  duration: number;
  uri: string;
  album: string;
  albumId: number;
  votes: number;

  queuedBy: string = "Host";
  playPosition: number = 0;
  isPlaying: boolean = false;

  constructor(){
  }

  create(data: any){
    this.name = data.name;
    this.imageUrl = data.album.images[1].url;
    this.imageUrlFull = data.album.images[0].url;
    this.id = data.id;
    this.uri = data.uri;
    this.artist = data.artists[0].name;
    this.artistId = data.artists[0].id;
    this.album = data.album.name;
    this.albumId = data.album.id;
    this.duration = data.duration_ms;
    this.votes = 0;

    return this;
  }

  private formatDuration(ms: number) {
    var minutes = Math.floor(ms / 60000);
    var seconds = Number(((ms % 60000) / 1000).toFixed(0));
    return (seconds == 60 ? (minutes+1) + ":00" : minutes + ":" + (seconds < 10 ? "0" : "") + seconds);
  }

  get durationSeconds() {
    return this.duration / 1000;
  }

  get formattedDuration(): String {
    return this.formatDuration(this.duration).toString();
  }

  get formattedPlayPosition(): String {
    return this.formatDuration(this.playPosition).toString();
  }

  setPlayPosition(ms: number) {
    this.playPosition = ms;
  }
}
