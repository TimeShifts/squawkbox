import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { HostDashboardPage } from '../host-dashboard/host-dashboard';
import { RoomService} from '../../services/room-service';
import { UserService} from '../../services/user-service';

@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html'
})

export class LandingPage {

  splash = true;

  constructor(public nav: NavController, public params: NavParams, public roomService: RoomService, public userService: UserService) { }

  ionViewDidLoad() { }

  goToHome(){
    this.nav.setRoot(HomePage);
  }
}
