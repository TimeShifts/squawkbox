import { Component, OnDestroy, ElementRef, Inject, EventEmitter } from '@angular/core';
import { NavController, NavParams, AlertController, PopoverController, ModalController } from 'ionic-angular';
import { AudioProvider } from 'ionic-audio';
import { MusicService } from '../../services/music-service';
import { RoomService } from '../../services/room-service';
import { AppService } from '../../services/app-service';
import { SettingsModal } from '../../pages/components/modals/settings-modal/settings-modal';
import { UsersModal } from '../components/modals/users-modal/users-modal';
import { HomePage } from '../home/home';
import { Track } from '../../models/track';
import { StatusBar } from '@ionic-native/status-bar';
import { BackgroundMode } from '@ionic-native/background-mode';
import { Platform } from 'ionic-angular';

@Component({
  selector: 'page-host-dashboard',
  templateUrl: 'host-dashboard.html'
})
export class HostDashboardPage implements OnDestroy {
  queue: any;
  clientCount: number = 1;
  currentUser: any;
  connection: any;
  currentTrack: Track;
  shuffle: boolean = false;
  showQueue: boolean = false;
  enableVoting: boolean = false;
  welcome: boolean = true;
  elementRef: ElementRef;
  musicSrvc: MusicService;
  miniPlayer: number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public appService: AppService, public musicService: MusicService, public roomService: RoomService, public alertCtrl: AlertController, public popoverCtrl: PopoverController, @Inject(ElementRef) elementRef: ElementRef, public statusBar: StatusBar, private backgroundMode: BackgroundMode, private plt: Platform) {
    this.queue = [];
    statusBar.styleLightContent();
    this.elementRef = elementRef;
    this.currentUser = navParams.get('user');
    this.musicSrvc = musicService;

    this.backgroundMode.enable();
  }

  public scrollFunction = (event: any) => {
    if(event.scrollTop >= 165) {
    switch(this.miniPlayer) {
      case 0: //Miniplayer is not init
        this.miniPlayer = 1;
        break;
      case 2: //Miniplayer is closed
        this.miniPlayer = 1; //open
        break
      }
    }
    else {
      switch(this.miniPlayer) {
        case 1: //Miniplayer is opened
          this.miniPlayer = 2; //close
          break;
      }
    }
  }

  ionViewDidLoad() {
    this.queue = [];
    //this.currentTrack = null;
    this.startListening();
  }

  startListening(){
    //Waiting for new connections
    this.connection = this.roomService.newConnection().subscribe(userId => {
      this.clientCount++;
      console.log("New Connection: Client Count =" + this.clientCount);
      this.roomService.clientSend(this.clientCount, userId);
      //Send client the current playing track
      this.roomService.sendPlaying(this.currentTrack, userId);

      //Send client the current queue
      this.roomService.sendQueue(this.queue, userId);

      //Send client queueing method
      this.roomService.toggleVoting(this.enableVoting);
    })

    //listen for adds to queue
    this.connection = this.roomService.queueAdd().subscribe(track => {
      console.log("Added to queue", track);
      this.addToQueue(track);
    })

    //listen for removes from queue
    this.connection = this.roomService.clientQueueRemove().subscribe(track => {
      console.log("Remove from queue", track);
      var obj = this.queue.find(myObj => myObj.id == track.id);
      this.deleteFromQueue(obj);
    })

    //listen for votes
    this.connection = this.roomService.voteRecieve().subscribe(track => {
      console.log("Vote recieved for", track);
      var obj = this.queue.find(myObj => myObj.id == track.id);
      obj.votes++;
      this.queue.sort((a, b) => a.votes < b.votes ? 1 : a.votes > b.votes ? -1 : 0);
    })

    //listen for client disconnect
    this.connection = this.roomService.clientDisconnected().subscribe(track => {
      console.log("Disconnect");
      this.clientCount--;
       console.log("New Count", this.clientCount);
    })

    //listen for client search
    this.connection = this.roomService.searchRequests().subscribe(search => {
      console.log("Search Request Term: ", search);
      this.musicService.getSearchResults(search.term, search.offset).then(data=>{
        this.roomService.searchSend(data, search.socketId);
      });
    })

     //listen for client suggested
    this.connection = this.roomService.suggestedRequests().subscribe(socketId => {
      console.log("Suggested Request");
      this.musicService.getSuggestedSongs(this.queue).then((data:any)=>{
        console.log(socketId);
        this.roomService.suggestedSend(data, socketId);
      });
    })

    this.connection = this.roomService.stayAliveRecieve().subscribe(id => {
      console.log("Staying aliveee-ivvee-iiiivee", id);
    })
  }
  /*
  Music Control
  */

  play(track) {
    //todo - play the passed in track
    this.setPlaying(track);
    console.log("Playing track", track);
  }

  playTopOfQueue() {
    console.log("Play top of queue");
    var firstTrack = this.queue[0];

    if (firstTrack != undefined) {
      if(this.shuffle){
        this.playRandomSong();
      }else{
        this.play(firstTrack);
      }

    } else {
        this.removePlaying();
        this.emptyQueueAlert();
    }
  }

  playRandomSong() {
    console.log("Playing random song!");
    if(this.queue.length > 1){
      var rand = this.queue[Math.floor(Math.random() * this.queue.length)];
      console.log("This is the track number: " + rand);
      this.play(rand);
    }
    else {
      //Play top track of queue
      this.play(this.queue[0]);
    }
  }

  setShuffle(shuffle) {
    this.shuffle = shuffle;
  }

  pauseCurrentTrack() {
    this.musicSrvc.pauseTrack(this.currentTrack);
  }

  onTrackFinished() {
    this.playTopOfQueue();
  }

  /*
 Playing Control
 */

  getPlaying(){
    return this.currentTrack;
  }

  setPlaying(track) {
    this.firstPlayShowQueue();
    this.removePlaying();
    this.currentTrack = track;
    track.isPlaying = true;
    //emit playing to room
    this.setPlayingforRoom(track);
    this.deleteFromQueue(track);
  }

  sendVote(track){
    this.roomService.sendVote(track);
  }

  setPlayingforRoom(track) {
    this.roomService.setPlaying(track);
  }

  removePlaying() {
    this.pauseCurrentTrack();
    this.currentTrack = null;
  }

  firstPlayShowQueue(){
    console.log("Show queue");

    if(!this.showQueue)

      this.showQueue = true;
    if(this.welcome)
      this.welcome = false;

    console.log(this.showQueue);
  }

  /*
 Queue Control
 */

  addToQueue(track) {
    this.queue.push(track);
    console.log(this.queue);
    this.autoPlayCheck();
  }

  deleteFromQueue(track) {
    let index = this.queue.indexOf(track);

    if (index > -1) {
      this.queue.splice(index, 1);
    }
    this.roomService.removeTrackFromQueue(track);
  }

  reorderQueue(indexes) {
    this.roomService.reorderQueue(indexes);
  }

  /*
  Helpers
  */

  private sendLibrary(userIdP){
    this.musicService.getLibrary().then((data)=>{
      this.roomService.sendLibrary(data, userIdP);
    }).catch(err =>{
       console.log('error getting library', err);
    });
  }

  private delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  getTrackById(id) {
    return this.queue.find(myObj => myObj.id == id);
  }

  autoPlayCheck() {
    if (this.currentTrack == null) {
      this.playTopOfQueue();
    }
  }

  ngOnDestroy() {
    console.log("Destoying");
    this.pauseCurrentTrack();
    this.roomService.leaveRoom();
    this.connection.unsubscribe();
  }

  /*
  DOM Control
  */

  emptyQueueAlert() {
    let alert = this.alertCtrl.create({
      title: 'Queue is empty',
      subTitle: 'Add tracks to queue',
      buttons: ['OK']
    });
    alert.present();
  }

  leaveRoom() {
    let alert = this.alertCtrl.create({
      title: 'Leaving?',
      subTitle: 'This will close the room. Are you sure?',
      buttons: [{
        text: 'Yes',
        handler: () => {
          this.leave();
        }
      },'Nope']
    });
    alert.present();
  }

  leave(){
    console.log("host leave");
    this.roomService.leaveRoom();
    this.navCtrl.setRoot(HomePage);
  }

  openSettings(myEvent){
    let myEmitter = new EventEmitter<boolean>();
    myEmitter.subscribe(
        v => {
          this.enableVoting = JSON.parse(v);
          console.log("Voting changed",this.enableVoting);
          this.roomService.toggleVoting(this.enableVoting);
        }
    );

    let popover = this.popoverCtrl.create(SettingsModal, { VotingEnabled: this.enableVoting, myEmitter: myEmitter });

    popover.present({
      ev: myEvent
    });
  }
}
