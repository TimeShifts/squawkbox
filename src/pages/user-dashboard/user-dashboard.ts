import { Component, OnDestroy } from '@angular/core';
import { NavController, NavParams, ModalController, PopoverController } from 'ionic-angular';
import { UserService } from '../../services/user-service';
import { RoomService } from '../../services/room-service';
import { AppService } from '../../services/app-service';
import { MusicService } from '../../services/music-service';
import { reorderArray, AlertController } from 'ionic-angular';
import { UsersModal } from '../components/modals/users-modal/users-modal';
import { HomePage } from '../home/home';
import { Track } from '../../models/track';
import { SettingsModal } from '../../pages/components/modals/settings-modal/settings-modal';

@Component({
  selector: 'page-user-dashboard',
  templateUrl: 'user-dashboard.html'
})

export class UserDashboardPage implements OnDestroy {

  currentUser: any;
  library: any;
  currentTrack: any;
  users: any;
  room: any;
  queue: any;
  connection: any;
  enableVoting: boolean = false;
  showQueue: boolean = false;
  welcome: boolean = true;
  clientCount: number;
  miniPlayer: number = 0;
  loading: boolean = true;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public modalCtrl: ModalController, public navParams: NavParams, public appService: AppService, public userService: UserService, public roomService: RoomService, public musicService: MusicService, public popoverCtrl: PopoverController) {
    this.currentUser = navParams.get('user');
    console.log(this.currentUser);

    //Join room
    this.roomService.joinRoom(this.currentUser.room, this.currentUser.id).then( success =>{
    }).catch(err =>{
      let alert = this.appService.createErrorAlert(err);
      alert.onDidDismiss(data => {
        this.navCtrl.setRoot(HomePage);
      });

    });;
  }

  public scrollFunction = (event: any) => {

    if(event.scrollTop >= 165) {
    switch(this.miniPlayer) {
      case 0: //Miniplayer is not init
        this.miniPlayer = 1;
        break;
      case 2: //Miniplayer is closed
        this.miniPlayer = 1; //open
        break
      }
    }
    else {
      switch(this.miniPlayer) {
        case 1: //Miniplayer is opened
          this.miniPlayer = 2; //close
          break;
      }
    }
  }

  ionViewDidLoad() {
    this.roomService.connectSocket();

    this.users = [];
    this.queue = [];

    //listen for client disconnect
    this.connection = this.roomService.clientDisconnected().subscribe(count => {
      this.clientCount--;
    })

    //listen for client connect
    this.connection = this.roomService.clientConnectRecieve().subscribe(count => {
      console.log("Recieved Client Count");
      this.clientCount = count;
    })

    this.connection = this.roomService.newConnection().subscribe(count => {
      this.clientCount++;
    })

    this.connection = this.roomService.hostDisconnected().subscribe(count => {
      this.hostDisconnectAlert();
    });

    //listen for changes
    this.connection = this.roomService.playing().subscribe(track => {
      this.showQueue = true;
      this.welcome = false;
      this.currentTrack = track;
    })

    this.connection = this.roomService.queueAdd().subscribe(track => {
      this.queue.push(track);
    })

    this.connection = this.roomService.queueRemove().subscribe(track => {
      var obj: any;
      obj = track;
      obj = this.queue.find(myObj => myObj.id == obj.id);

      let index = this.queue.indexOf(obj);
      if (index > -1) {
        this.queue.splice(index, 1);
      }
    })

    //listen for votes
    this.connection = this.roomService.voteRecieve().subscribe(track => {
      console.log("Vote recieved for", track);
      var obj = this.queue.find(myObj => myObj.id == track.id);
      obj.votes++;
      this.queue.sort((a, b) => a.votes < b.votes ? 1 : a.votes > b.votes ? -1 : 0)
    })

    this.connection = this.roomService.queueReorder().subscribe((indexes: any) => {
      console.log(indexes);
      this.queue = reorderArray(this.queue, indexes);
    })

    this.connection = this.roomService.playingRecieve().subscribe(playing => {

      if(playing != null){
        this.currentTrack = playing;
        this.showQueue = true;
        this.welcome = false;
      }
      this.loading = false;
    })

    this.connection = this.roomService.queueRecieve().subscribe(queue => {
      this.queue = queue;
    })

    this.connection = this.roomService.libraryRecieve().subscribe(library => {
      this.musicService.library = library;
    })

    this.connection = this.roomService.stayAliveRecieve().subscribe((id: any) => {
      console.log("Staying aliveee-ivvee-iiiivee");
    })

    this.connection = this.roomService.toggleVotingRecieve().subscribe(voting => {

      let voting_result: boolean = JSON.parse(voting);
      console.log("Recieved voting toggle ", voting_result);
      this.enableVoting = voting_result;
    });

    this.connection = this.roomService.hostReconnected().subscribe(data => {
      //Remove modal
    });

  }

  sendVote(track){
    this.roomService.sendVote(track);
  }

  addToQueue(track) {
    this.welcome = false;
    this.queue.push(track);
  }

  deleteFromQueue(track) {
    let index = this.queue.indexOf(track);

    if (index > -1) {
      this.queue.splice(index, 1);
    }
    this.roomService.clientRemoveTrackFromQueue(track);
  }

  leaveRoom() {
    let alert = this.alertCtrl.create({
      title: 'Leaving?',
      subTitle: 'Are you sure?',
      buttons: [{
        text: 'Yes',
        handler: () => {
          this.toHome();
        }
      },'Nope']
    });
    alert.present();
  }

  hostDisconnectAlert(){
    let alert = this.alertCtrl.create({
      title: 'Session Ended',
      subTitle: 'You will be returned to the main menu',
      buttons: [{
        text: 'Okay',
        handler: () => {
          this.toHome();
        }
      }]
    });
    alert.present();
  }

  openUsersModal(){
    let modal = this.modalCtrl.create(UsersModal, {room: this.currentUser.room});
    modal.present();
  }

  toHome(){
    this.navCtrl.setRoot(HomePage);
  }

  ngOnDestroy() {
    console.log("Destroying i");
    this.roomService.leaveRoom();
    this.navCtrl.setRoot(HomePage);
    this.connection.unsubscribe();
  }
}
