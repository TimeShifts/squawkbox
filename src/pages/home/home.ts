import { Component } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { NavController, ModalController, AlertController, Platform } from 'ionic-angular';
import { UserDashboardPage } from '../user-dashboard/user-dashboard';
import { JoinRoomModal } from '../components/modals/join-room-modal/join-room-modal';
import { CreateRoomModal } from '../components/modals/create-room-modal/create-room-modal';
import { HelpModal } from '../components/modals/help-modal/help-modal';
import { UserService } from '../../services/user-service';
import { RoomService } from '../../services/room-service';
import { AuthService } from '../../services/auth-service';
import { AppService } from '../../services/app-service';
import { StatusBar } from '@ionic-native/status-bar';
import { BackgroundMode } from '@ionic-native/background-mode';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  users: any;
  rooms: any;
  name: any;
  hasConnection: boolean;
  hasLocalRoom: boolean;
  allowCreate: boolean;
  localRoom: any;
  checkRoomTimer: any;
  splash = this.platform.is('cordova') ? false : true;
  slides: boolean; //See if name has already been set, if so then...

  constructor(public platform: Platform, public nav: NavController, public appService: AppService ,public userService: UserService, public roomService: RoomService,public authService: AuthService, public modalCtrl: ModalController, public alertCtrl: AlertController, public statusBar: StatusBar, private backgroundMode: BackgroundMode) {
    this.name = '';

    statusBar.styleLightContent();

    //Run app in background
    this.backgroundMode.enable();

    //Skip slides if name is set
    this.skipSlidesCheck();

    let timer = Observable.timer(0,30000);
    this.checkRoomTimer = timer.subscribe(t => {this.checkNetwork(t)});
  }

  ionViewDidLoad() {
    setTimeout(() => this.splash = false, 4000);

    if (this.platform.is('cordova')) {
        this.allowCreate = true;
    }else{
        this.allowCreate = false;
    }
  }

  checkNetwork(t){
    console.log("Checking");
    this.roomService.roomOnNetworkCheck().then(room =>{
      this.localRoom = room;
      this.hasLocalRoom = true;
      this.hasConnection = true;
    }).catch(err =>{
      if(err == "Connection error"){
        this.appService.showToast("Error establishing connection to server");
        this.hasConnection = false;
      }else{
        this.hasLocalRoom = false;
        this.hasConnection = true;
      }

    });
  }

  stopCheckRoomTimer(){
    this.checkRoomTimer.unsubscribe();
  }

  skipSlidesCheck(){
    this.appService.getItem("name").then(name =>{
      if(name != null){
        this.name = name;
        this.slides = false;
      }else
        this.slides = true;
    }).catch( err => {
      this.slides = true;
    });
  }



  joinRoom() {
    if(!this.hasConnection) return;

    let modal = this.modalCtrl.create(JoinRoomModal);

    modal.onDidDismiss(room => {
      if (room) {
        this.join(room);
      }
    });

    modal.present();
  }

  joinLocalRoom(){
    this.join(this.localRoom);
  }

  join(roomP){
    this.roomService.getRoom(roomP).then((room) => {
        this.userService.createUser(room).then(user =>{
          this.stopCheckRoomTimer();
          this.nav.setRoot(UserDashboardPage, {
            user: this.userService.user
          });
        });
    }).catch((err) => { this.showAlert(); });
  }

  createRoom() {
    if(!this.hasConnection || !this.allowCreate) return;


    if(!this.appService.isTokenExpired()){
      this.authService.spotifyAuth().then(data=>{
        console.log(data);
      });
    }else{
       let modal = this.modalCtrl.create(CreateRoomModal);

        modal.onDidDismiss(value =>{
          if(value){
            this.authService.spotifyAuth().then(data=>{
              this.stopCheckRoomTimer();
              console.log(data);
            });
          }
        });

        modal.present();
    }
  }

  help(){
    let modal = this.modalCtrl.create(HelpModal);

    modal.onDidDismiss(room => {

    });

    modal.present();
  }

  /*
  DOM Control
  */

  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'No Room Found!',
      subTitle: 'Double check that you entered the room code correctly',
      buttons: ['OK']
    });
    alert.present();
  }

  showDisconnectAlert() {
    let alert = this.alertCtrl.create({
      title: 'Connection Error!',
      subTitle: 'Error connecting to Squawkbox. Try again later.',
      buttons: ['OK']
    });
    alert.present();
  }

  finishSlides() {
    //Set name
    this.appService.setItem("name", this.name);
    this.slides = false;
  }

  // DEV ONLY
  clearStorage(){
    localStorage.clear(); //ONLY FOR DEV
  }
}
