import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { RoomService } from '../../services/room-service';
import { UserService } from '../../services/user-service';
import { MusicService } from '../../services/music-service';
import { StatusBar } from '@ionic-native/status-bar';
import { Platform } from 'ionic-angular';

declare var window: any;

@Component({
  selector: 'page-browse',
  templateUrl: 'browse.html'
})
export class BrowsePage {

  browse = "library";
  searchToggle: boolean;
  searchInput: string;
  search: string;
  queue: any = [];
  showSuggested: boolean;
  suggestedTracks: any[];
  extraSuggested: any[];
  iPhoneX: boolean = false;
  replaceIndex: number = null;
  statBar: any;
  connection: any;

  constructor( public view: ViewController, public roomService: RoomService, public userService: UserService, private musicService: MusicService, params: NavParams, public statusBar: StatusBar, public platform: Platform) {
    this.queue = params.get('queue')? params.get('queue'): [];
    this.iPhoneX = window.iphoneX;
    this.extraSuggested = [];
    this.statBar = statusBar;
    var delay = this;
    if(platform.is('ios')){
      setTimeout(function () { delay.statBar.styleDefault() }, 250);
    }

    //listen for host search results
    if(!this.roomService.isHost){
      this.connection = this.roomService.suggestedResults().subscribe(songs =>{
        console.log("Got suggested songs", songs);
        this.showSuggested = true;
        this.setSuggestedTracks(songs);
      });
    }

  }

  ionViewDidLoad() {
    this.showSuggested = false;
    this.getSuggestions();
  }

  replaceSuggested(replaceIndex){
    this.replaceIndex = replaceIndex;
    if(this.extraSuggested.length == 0){
      this.getSuggestions();
    }else{
      this.suggestedTracks[this.replaceIndex] = this.extraSuggested.splice(0,1)[0];
      this.replaceIndex = null;
    }
  }

  getSuggestions(){
    if(this.queue && this.queue.length > 0){
      if(this.roomService.isHost){
        this.musicService.getSuggestedSongs(this.queue).then((data:any)=>{
          this.showSuggested = true;
          this.setSuggestedTracks(data);
        });
      }else{
         console.log("Get Suggested songs");
          this.roomService.suggested();
      }
    }
  }

  setSuggestedTracks(tracks){
    console.log(tracks);
    if(this.replaceIndex != null){
      var replacementTrack =  tracks.splice(0,1)[0];
      console.log(this.suggestedTracks[this.replaceIndex], replacementTrack);
      this.suggestedTracks[this.replaceIndex] = replacementTrack;
      this.extraSuggested = tracks;
      console.log("Suggested song replaced with", replacementTrack);
      this.replaceIndex = null;
    }else{
      console.log("Suggested songs", tracks);
      this.suggestedTracks = tracks;
    }
  }

  onInput(newSearch){
    console.log(newSearch, this.searchInput);
    if(this.searchInput != undefined){
       this.search = this.searchInput;
    }
  }

  onCancel(){
    this.search = "";
  }

  toggleSearchBar(){
    this.searchToggle = ! this.searchToggle;
  }

  addToQueue(track){
    console.log(track);
    this.roomService.addToQueue(track, this.userService.user.name, this.userService.user._id);
  }

  removeFromQueue(track){
    var obj = this.queue.find(myObj => myObj.id == track.id);
    let index = this.queue.indexOf(obj);

    if (index > -1) {
      this.queue.splice(index, 1);
    }
  }

  close(){
    this.view.dismiss();
    var delay = this;
    setTimeout(function () { delay.statBar.styleLightContent(); }, 50);
  }

}
