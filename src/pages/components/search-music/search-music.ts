import { Component, Input, EventEmitter, Output } from '@angular/core';
import { MusicService } from '../../../services/music-service';
import { AppService } from '../../../services/app-service';
import { RoomService } from '../../../services/room-service';
import { StatusBar } from '@ionic-native/status-bar';

@Component({
  selector: 'search-music',
  templateUrl: 'search-music.html',
  providers: []
})
export class SearchMusicComponent {
  @Input() show: boolean;
  @Input() showSuggested: boolean;
  @Input() suggestedTracks: any;
  @Input() queue: any;
  @Output() trackAdded = new EventEmitter();
  @Output() replaceSuggested = new EventEmitter();
  @Output() moreTracks = new EventEmitter();
  public _search: string = "";
  clean: boolean = true;
  offset: number;
  loading: boolean = false;
  infiniteScroll: any;
  searchResults: any[];
  connection: any;

  constructor(public musicService: MusicService, public appService: AppService, public roomService: RoomService) {
    this.searchResults = [];
    this.loading = false;
    //listen for host search results
    this.connection = this.roomService.searchResults().subscribe(data =>{
      this.pushSongs(data);
      if(this.infiniteScroll){
        this.infiniteScroll.complete();
      }
    })
  }

  get search(): string {
    // transform value for display
    return this._search;
  }

  @Input()
  set search(search: string) {
    if(search){
      this._search = search;
      this.offset = 0;
      this.getSearchResults();
    }
  }

  getMoreSongs(event){
    this.infiniteScroll = event;
    console.log("Get more songs");
    this.offset = this.searchResults.length;
    this.getSearchResults().then(function(){
      this.infiniteScroll.complete();
    });
  }

  addToQueue(track, isSuggestion?){
    var duplicate = false;

    console.log("Add to queue", this.queue);
    this.queue.forEach(function (queuedTrack) {
      if(queuedTrack.id == track.id){
        duplicate = true;
      }
    });

    console.log(duplicate);
    if(!duplicate){
      this.appService.showTrackAddToast();
      this.trackAdded.emit(track);

      console.log(isSuggestion);
      if(isSuggestion){
        this.replaceSuggested.emit(this.suggestedTracks.indexOf(track));
      }

    }else{
      this.appService.showToast("Track is already in the queue");
    }

    
  }

  getSearchResults(){
    return new Promise((resolve, reject) => {
      this.clean = false;
      
      if(this._search != undefined && new String(this._search).length > 2){
        if(!this.offset){
          this.loading = true;
        }
        if(this.roomService.isHost){
          console.log("searching with offset", this.offset);
          this.musicService.getSearchResults(this._search, this.offset).then(data =>{
            console.log(data);
              this.pushSongs(data);
              resolve();
          }).catch(err =>{
            
              reject();
          });
        }else{
          this.roomService.search(this._search, this.offset);
        }
      }
    });
  }

  private pushSongs(songs, offset?){
    this.loading = false;
    if(!this.offset){
      this.searchResults = [];
    }
    var results:any = songs;
    for(let result of results){
        this.searchResults.push(result);
    }
  }


}
