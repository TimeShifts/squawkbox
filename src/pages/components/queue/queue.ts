import { Component, Input, EventEmitter, Output } from '@angular/core';
import { RoomService } from '../../../services/room-service';
import { BrowsePage } from '../../../pages/browse/browse';
import { reorderArray, NavController, ModalController } from 'ionic-angular';

@Component({
  selector: 'queue',
  templateUrl: 'queue.html',
  styles: [`
    .play {
      position: absolute;
      top: 0rem;
      display: inline-flex;
      min-width: inherit;
      min-height: inherit;
      margin: 8px 16px 8px 0;
      background: #282726;
      opacity: .8;
    }
    .playbutton {
      width: 100% !important;
      height: inherit !important;
      margin: 0 !important;
      color: #FFFFFF;
      opacity: .8;

      ion-icon {
        font-size: 2em !important;
      }
    }
  `],
  providers: []
})
export class QueueComponent {
  @Input() queue: any;
  @Input() host: boolean;
  @Input() showQueue: boolean;
  @Input() enableVoting: boolean;
  @Input() userId: string;
  @Output() trackRemoved = new EventEmitter();
  @Output() playTrack = new EventEmitter();
  @Output() reorderQueue = new EventEmitter();
  @Output() sendVote = new EventEmitter();

  reorder: boolean = false;
  reorderText: string = "Edit";
  editToggle: boolean = false;
  voted: string[] = new Array<string>();

  constructor(public roomService: RoomService, public navCtrl: NavController, public modalCtrl: ModalController) {

  }

  reorderToggle() {
    this.reorder = !this.reorder;
    if (this.reorder)
      this.reorderText = "Done";
    else
      this.reorderText = "Edit";
  }

  vote($event){
    console.log("Event clicked!");

    if(this.voted.indexOf($event.id) === -1 && this.enableVoting) {
      console.log("Voted!");
      this.voted.push($event.id);
      this.sendVote.emit($event);
    }
  }

  deleteFromQueue(track) {
    this.trackRemoved.emit(track);
  }

  play($event) {
    this.playTrack.emit($event);
  }

  reorderItems(indexes) {
    this.reorderQueue.emit(indexes);
    this.queue = reorderArray(this.queue, indexes);
  }

  browse(){
    let browseModal = this.modalCtrl.create(BrowsePage, {queue: this.queue ? this.queue : []});
    browseModal.present();
  }

  edit() {
    this.editToggle = !this.editToggle;
  }
}
