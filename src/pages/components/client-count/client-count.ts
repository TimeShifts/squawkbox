import { Component, Input, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'client-count',
  templateUrl: 'client-count.html',
  providers: []
})
export class ClientCountComponent {
  @Input() clientCount: number;
  @Output() openModal = new EventEmitter();

  constructor() {
    
  }

  open(){
    this.openModal.emit();
  }

  
}