import { Component, Input, EventEmitter, Output } from '@angular/core';
import { MusicService } from '../../../services/music-service';
import { AppService } from '../../../services/app-service';

@Component({
  //moduleId: module.id + "",
  selector: 'library',
  templateUrl: 'library.html',
  providers: []
})
export class LibraryComponent {
  @Input() library: any;
  @Input() search: any = '';
  @Output() trackAdded = new EventEmitter();

  myTracks: any[];

  constructor(public musicService: MusicService, public appService: AppService) {
    this.library = [];
    this.appService.showLoading("Loading Library...");

    //Get library
    this.musicService.getLibrary().then((data) => {
      this.library = data;
      this.appService.stopLoading();
    }).catch((err)=>{
      console.log('error with getting library', err);
    });

  }

  addTrack(track) {
    this.appService.showTrackAddToast();
    this.trackAdded.emit(track);
  }
}
