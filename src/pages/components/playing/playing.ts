import { Component, Input, EventEmitter, Output } from '@angular/core';
import { ModalController } from 'ionic-angular';
import { RoomService } from '../../../services/room-service';
import { MusicService } from '../../../services/music-service';
import { BrowsePage } from '../../../pages/browse/browse';
import { Track } from '../../../models/track';
import { Observable } from "rxjs/Observable";

declare var cordova: any;

@Component({
  //moduleId: module.id + "",
  selector: 'playing',
  templateUrl: 'playing.html',
  providers: []
})
export class PlayingComponent {
  private _currentTrack: Track;
  @Input() shuffle: boolean = false;
  @Input() host: boolean;
  @Input() welcome:boolean = true;
  @Input() clientCount: number;
  @Output() trackFinished = new EventEmitter();
  @Output() playNextTrack = new EventEmitter();
  @Output() shuffleLib = new EventEmitter();


  public timer: Observable<any>;
  public subscription: any;
  public startedSeeking: boolean;
  public startedSeekingTime: number = 0;
  public connection: any;
  public isPaused: boolean = false;

  musicSrvc: MusicService;

  constructor(public roomService: RoomService, public modalCtrl: ModalController, public musicService: MusicService) {
    this.musicSrvc = musicService;
    this.initializeTimer();
  }

  get currentTrack(): Track {
    return this._currentTrack;
  }

  @Input('currentTrack')
  set currentTrack(currentTrack: Track) {
    this._currentTrack = currentTrack;

    if(this._currentTrack != null) {
      if(this.host){
        this.musicSrvc.playTrack(this._currentTrack);
        this.pauseTimer(); //Reset timer
        this.startTimer(); //Start timer  
      }

      if(!this.host && this._currentTrack.isPlaying){
        this.pauseTimer(); //Reset timer
        this.startTimer(); //Start timer  
      }
      
      this.setSocketListening();
    }
  }

  toggle() {
    if(this._currentTrack != null) {
      if(this._currentTrack.isPlaying) {
        this.pauseTimer();
        this.roomService.trackPause();
        if(this.host) this.musicSrvc.pauseTrack(this._currentTrack);
      }
      else {
        this.startTimer();
        this.roomService.trackPlay();
        if(this.host) this.musicSrvc.resumeTrack(this._currentTrack);
      }
    }
  }

  playNext() {
    this.pauseTimer();
    this.playNextTrack.emit();
  }

  shuffleLibrary() {
    this.shuffle = !this.shuffle;
    this.shuffleLib.emit(this.shuffle);
  }

  onTrackFinished($event) {
    this.trackFinished.emit($event);
  }

  browse(){
    let browseModal = this.modalCtrl.create(BrowsePage);
    browseModal.present();
  }

  scrubTrack(track) {
    this._currentTrack.playPosition = track.value;
    this.pauseTimer();
    this.roomService.trackScrub(track.value);

    if(this._currentTrack.isPlaying) {
      if(this.host) this.musicSrvc.seekTo(track.value, this._currentTrack);
      this.startTimer();
    }
    else {
      if(this.host) this.musicSrvc.seekToOnPause(track.value, this._currentTrack);
      this.startTimer();
    }
  }

  onIndexChange(track) {
    let offset = this.startedSeekingTime + 5000;
    if(track.value > offset && offset != 5000) {
      this.pauseTimer();
    }

    this.startedSeekingTime = track.value;
  }

  swiped(e) {
    if(this.host) {
      switch (e.offsetDirection) {
        case 2: this.playNext(); break;
      }
    }
  }

  startTimer() {
    let offset = (this._currentTrack.playPosition / 1000);
    this.timer = Observable.timer(0, 1000)
    .map(value => value + offset)
    .takeWhile(value => this._currentTrack != null && value <= (this._currentTrack.duration / 1000) + 1);

    this.subscription = this.timer.subscribe(t => {
      var current = (t * 1000);

      this._currentTrack.playPosition = current;
      var duration = Math.round(this._currentTrack.duration);

      if(this._currentTrack.duration != 0 && current >= duration) {
        this.playNext();
      }
    });
  }

  pauseTimer() {
    this.subscription.unsubscribe();
  }

  initializeTimer() {
    this.timer = Observable.timer(0, 0);
    this.subscription =this.timer.subscribe(t => {});
    this.subscription.unsubscribe();
  }

  setSocketListening(){
    if(!this.roomService.isHost && this.connection == null){

      console.log("Listening track srub");
      this.connection = this.roomService.trackPlayRecieve().subscribe(play => {
        console.log("Recieved track play click");
         this.pauseTimer();
         this.startTimer();
        //Start timer
      })

      this.connection = this.roomService.trackPauseRecieve().subscribe(pause => {
        console.log("Recieved track pause click");
        this.pauseTimer();
        //Stop timer
      })

      this.connection = this.roomService.trackScrubRecieve().subscribe(scrub => {
        console.log("Recieved track scrub", scrub);
        this._currentTrack.playPosition = scrub;
        if(this._currentTrack.isPlaying){
          console.log("PLAYING");
          this.pauseTimer();
          this.startTimer();
        }

        //Scrub value change
      })
    }
  }
}

