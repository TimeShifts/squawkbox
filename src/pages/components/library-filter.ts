import {Pipe, PipeTransform} from '@angular/core'

@Pipe({
    name: "search",
    pure: false
})export class LibrarySearch implements PipeTransform{
    transform(data: any[], term: string){
        if(!data) return [];
        term = term.toLowerCase();
        return data.filter((item)=> {
           for (let key in item ) {
             if((""+item[key]).toLowerCase().includes(term)){
                return true;
             }
           }
           return false;
        });
    }
}