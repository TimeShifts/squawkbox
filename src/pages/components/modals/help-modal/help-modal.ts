import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { Platform } from 'ionic-angular';

@Component({
  selector: 'help-modal',
  templateUrl: 'help-modal.html'
})
export class HelpModal {
  statBar: any;

  constructor(public viewCtrl: ViewController, public statusBar: StatusBar, public platform: Platform) {
    statusBar.styleDefault();
    this.statBar = statusBar;
    var delay = this;
    if(platform.is('ios')){
      setTimeout(function () { delay.statusBar.styleDefault() }, 250);
    }
  }

  close(): void {
    this.viewCtrl.dismiss();
    var delay = this;
    setTimeout(function () { delay.statBar.styleLightContent(); }, 50);
  }
}
