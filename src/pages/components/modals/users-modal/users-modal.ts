import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { UserService } from '../../../../services/user-service';
@Component({
  templateUrl: 'users-modal.html'
})
export class UsersModal { 

  room: any;
  users: any;

  constructor(public viewCtrl: ViewController, public navParams: NavParams, public userService: UserService) {
    this.room = navParams.get("room");

    this.userService.getUsersInRoom(this.room).then((data) => {
      console.log(data);
      this.users = data;
    });
  }
 
  close(): void {
    this.viewCtrl.dismiss();
  }
}