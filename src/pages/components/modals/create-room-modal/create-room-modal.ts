import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { MusicService } from '../../../../services/music-service';
import { StatusBar } from '@ionic-native/status-bar';
import { Platform } from 'ionic-angular';

@Component({
  selector: 'create-room-modal',
  templateUrl: 'create-room-modal.html'
})
export class CreateRoomModal {
  statBar: any;

  constructor(public viewCtrl: ViewController, public musicService: MusicService, public statusBar: StatusBar, public platform: Platform) {
    statusBar.styleDefault();
    this.statBar = statusBar;
    var delay = this;
    if(platform.is('ios')){
      setTimeout(function () { delay.statBar.styleDefault() }, 250);
    }
  }

  create(): void {
    this.viewCtrl.dismiss(true);
  }

  close(): void {
    this.viewCtrl.dismiss(false);
    var delay = this;
    setTimeout(function () { delay.statBar.styleLightContent(); }, 50);
  }
}
