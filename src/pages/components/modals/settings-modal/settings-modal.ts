import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';


@Component({
  templateUrl: 'settings-modal.html'
})
export class SettingsModal {
  private _anEmitter: EventEmitter<boolean>;
  voting: boolean;

  constructor(public viewCtrl: ViewController, public navParams: NavParams) {
    this.voting = this.navParams.get('VotingEnabled');
    this._anEmitter = this.navParams.get('myEmitter');
  }

  close(): void {
    this.viewCtrl.dismiss();
  }

  setVoting() {
    this._anEmitter.emit(this.voting);
  }
}
