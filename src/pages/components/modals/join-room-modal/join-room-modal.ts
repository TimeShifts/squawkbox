import { Component } from '@angular/core';
import { ViewController, NavController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { UserDashboardPage } from '../../../user-dashboard/user-dashboard';
import { RoomService } from '../../../../services/room-service';
import { UserService } from '../../../../services/user-service';
import { StatusBar } from '@ionic-native/status-bar';
import { Observable } from 'rxjs/Rx';
import { Platform } from 'ionic-angular';

declare var window: any;

@Component({
  selector: 'join-room-modal',
  templateUrl: 'join-room-modal.html'
})
export class JoinRoomModal {
  room: String = "";
  connection: any;
  hasConnection: boolean;
  hasLocalRoom: boolean;
  hasDeclinedAutoJoin: boolean;
  localRoom: any;
  statBar: any;

  constructor(public viewCtrl: ViewController, public nav: NavController, public roomService: RoomService, public userService: UserService, public toastCtrl: ToastController, public statusBar: StatusBar, public platform: Platform) {
    this.checkNetwork(null);
    this.statBar = statusBar;
    var delay = this;

    if(platform.is('ios')){
      setTimeout(function () { delay.statBar.styleDefault() }, 250);
    }
  }

  join(): void {
    this.viewCtrl.dismiss(this.room);
  }

  joinLocalRoom(){
    this.joinLocal(this.localRoom);
  }

  joinLocal(roomP){
    this.roomService.getRoom(roomP).then((room) => {
        this.userService.createUser(room).then(user =>{
          this.nav.setRoot(UserDashboardPage, {
            user: this.userService.user
          });
        });
    }).catch((err) => { console.log("Couldn't connect") });
  }

  declineAutoJoin() {
    this.hasDeclinedAutoJoin = !this.hasDeclinedAutoJoin;
  }

  checkNetwork(t){
    console.log("Checking");
    this.roomService.roomOnNetworkCheck().then(room =>{
      this.localRoom = room;
      this.hasLocalRoom = true;
      this.hasConnection = true;

      let toast: any;

      if(window.iphoneX == true) {
        toast = this.toastCtrl.create({
          message: 'A room was found on your network: ' + this.localRoom,
          duration: 3000,
          cssClass: "iphonex-toast"
        });
      }
      else {
        toast = this.toastCtrl.create({
          message: 'A room was found on your network: ' + this.localRoom,
          duration: 3000
        });
      }

      //toast.present();
    }).catch(err =>{
      if(err == "Connection error"){
        this.hasConnection = false;
      }else{
        this.hasLocalRoom = false;
        this.hasConnection = true;
      }
     });
  }

  close(): void {
    this.viewCtrl.dismiss();
    var delay = this;
    setTimeout(function () { delay.statBar.styleLightContent(); }, 50);
  }

  setUpperCase(room): void {
    var output = room.toUpperCase();
    var re = /^[A-Z]{0,6}$/;

    if (!re.test(output)) {
      output = output.slice(0, -1);
    }

    this.room = output;
  }
}


