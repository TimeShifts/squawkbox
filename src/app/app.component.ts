import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav, Config } from 'ionic-angular';
import { HomePage } from '../pages/home/home';
import { LandingPage } from '../pages/landing/landing';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

declare var window: any;

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  // make HomePage the root (or first) page
  rootPage: any;
  pages: Array<{title: string, component: any}>;
  iPhoneX: boolean = false;

  constructor(
    public platform: Platform,
    public menu: MenuController,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public config: Config
  ) {
    this.initializeApp();

    var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    var ratio = window.devicePixelRatio || 1;
    var screen = {
      width : window.screen.width * ratio,
      height : window.screen.height * ratio
    };

    // iPhone X Detection
    if (iOS && screen.width == 1125 && screen.height === 2436) {

      // Set a global variable now we've determined the iPhoneX is true
      window.iphoneX = true;
      this.iPhoneX = window.iphoneX;
    }
    else {
      window.iphoneX = false;
    }

    if (!this.platform.is('cordova')){
      window.iphoneX = false;
      this.iPhoneX = false;
      this.rootPage = LandingPage;
    }else  {
      this.rootPage = HomePage;
    }
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      //this.statusBar.overlaysWebView(false);
      //this.statusBar.hide();
      //this.statusBar.backgroundColorByName("black");
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component);
  }
}
