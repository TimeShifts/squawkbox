import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';
import { LandingPage } from '../pages/landing/landing';
import { HomePage } from '../pages/home/home';
import { UserDashboardPage } from '../pages/user-dashboard/user-dashboard';
import { HostDashboardPage } from '../pages/host-dashboard/host-dashboard';
import { BrowsePage } from '../pages/browse/browse';
import { HelpModal } from '../pages/components/modals/help-modal/help-modal';
import { UsersModal } from '../pages/components/modals/users-modal/users-modal';
import { JoinRoomModal } from '../pages/components/modals/join-room-modal/join-room-modal';
import { SettingsModal } from '../pages/components/modals/settings-modal/settings-modal';
import { QueueComponent } from '../pages/components/queue/queue';
import { PlayingComponent } from '../pages/components/playing/playing';
import { LibraryComponent } from '../pages/components/library/library';
import { ClientCountComponent } from '../pages/components/client-count/client-count';
import { SearchMusicComponent } from '../pages/components/search-music/search-music';
import { LibrarySearch } from '../pages/components/library-filter';
import { SpinnerComponent } from '../pages/components/spinner/spinner';
import { MillesecondsToTimePipe } from '../pages/pipes/milliseconds-time';
import { CreateRoomModal } from '../pages/components/modals/create-room-modal/create-room-modal';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { BackgroundMode } from '@ionic-native/background-mode';
import { PowerManagement } from '@ionic-native/power-management';
import { UserService } from '../services/user-service';
import { RoomService } from '../services/room-service';
import { MusicService } from '../services/music-service';
import { AppService } from '../services/app-service';
import { ApiService } from '../services/api-service';
import { AuthService } from '../services/auth-service';
import { HostService } from '../services/host-service';
import { Track } from '../models/track';

//import { CloudSettings, CloudModule } from '@ionic/cloud-angular';
import { IonicStorageModule } from '@ionic/storage';

/*const cloudSettings: CloudSettings = {
  'core': {
    'app_id': '372a6091'
  }
};*/

@NgModule({
  declarations: [
    MyApp,
    LandingPage,
    HomePage,
    UserDashboardPage,
    HelpModal,
    UsersModal,
    JoinRoomModal,
    CreateRoomModal,
    SettingsModal,
    HostDashboardPage,
    BrowsePage,
    QueueComponent,
    PlayingComponent,
    LibraryComponent,
    ClientCountComponent,
    SearchMusicComponent,
    LibrarySearch,
    MillesecondsToTimePipe,
    SpinnerComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp, {
      platforms : {
        ios : {
          // These options are available in ionic-angular@2.0.0-beta.2 and up.
          scrollAssist: false,    // Valid options appear to be [true, false]
          autoFocusAssist: false  // Valid options appear to be ['instant', 'delay', false]
        }
        // http://ionicframework.com/docs/v2/api/config/Config/)
      }
    }),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LandingPage,
    HomePage,
    UserDashboardPage,
    HelpModal,
    UsersModal,
    JoinRoomModal,
    CreateRoomModal,
    SettingsModal,
    HostDashboardPage,
    BrowsePage,
    QueueComponent,
    PlayingComponent,
    LibraryComponent,
    SearchMusicComponent,
    ClientCountComponent,
    SpinnerComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    UserService,
    HostService,
    RoomService,
    MusicService,
    AppService,
    ApiService,
    AuthService,
    InAppBrowser,
    BackgroundMode,
    PowerManagement,
    Track,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
