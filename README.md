> Project Version 1.0

<div  align="center">
<img  alt="react-sketchapp"  src="bird.png"  style="width:160; height: 160;"  />
</div>

<div  align="center">
[https://squawkbox.app/](https://squawkbox.app/)
</div>
<div  align="center">
<strong>Squawkbox</strong> is your own virtual jukebox. Invite your friends to queue up songs
</div>
<div  align="center">
[App Store](https://squawkbox.app/) • [Google Play](https://squawkbox.app/)
</div>

## Quickstart 🏃‍

### Dependencies

*  [npm & node.js](https://nodejs.org/en/download/).
*  [MongoDB](https://www.mongodb.com/)
*  [Ionic & Ionic-CLI](https://ionicframework.com/)

Then in a terminal:

```bash
git clone git@gitlab.com:TimeShifts/squawkbox.git

cd squawkbox/ && npm install

node server.js ##Runs Server

ionic serve --lab ##Runs Client
```

[![Gitter](https://img.shields.io/gitter/room/octobox/octobox.svg)](https://gitter.im/squawkbox-app)
[![license](https://img.shields.io/github/license/octobox/octobox.svg)](LICENSE.txt) 

## Authors
This project is a [TimeShifts]() project. The main contributers are:
* [@coreyweber11]() - Corey Weber
* [@cmacrowther]() - Colin Crowther

## Contributing
Please do! Our source code is currently only hosted on GitLab.

If you want to contribute but don't know where to start, take a look at our issue log. If you would like to become a maintainer, feel free to ask.


### Merge Requests
-   Fork the project.
-   Make your feature addition or bug fix off of `dev` branch.
-   Send a pull request. Please try to use `issueXX` branches.
  
## Licensing
We use the [GNU GPL V3 License](LICENSE.txt)
  

## Copyright
© 2018 TimeShifts
